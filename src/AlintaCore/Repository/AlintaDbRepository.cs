﻿using AlintaCore.Interface;
using AlintaInfrastructure.Data;
using AlintaInfrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlintaCore.Repository
{
    public class AlintaDbRepository : IAlintaRepository
    {
        private readonly AlintaContext _db;

        public AlintaDbRepository(AlintaContext dbContext)
        {
            _db = dbContext;
        }

        public async Task<bool> AnyCustomer(int id)
        {
            return await _db.Customers
                .AsNoTracking()
                .AnyAsync(x => x.Id == id);
        }

        public async Task<int> CreateCustomer(Customer customer)
        {
            _db.Customers.Add(customer);
            await _db.SaveChangesAsync();
            return customer.Id;
        }

        public async Task<bool> DeleteCustomer(int id)
        {
            Customer customer = new Customer() { Id = id };
            _db.Entry(customer).State = EntityState.Deleted;
            await _db.SaveChangesAsync();
            return true;
        }

        public async Task<Customer> GetCustomer(int id)
        {
            return await _db.Customers
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Customer>> SearchCustomer(string name)
        {
            return await _db.Customers
                .AsNoTracking()
                .Where(x => x.FirstName.Equals(name, StringComparison.OrdinalIgnoreCase) 
                  || x.LastName.Equals(name))
                .ToListAsync();
        }

        public async Task<bool> UpdateCustomer(Customer customer)
        {
            var existingCustomer = _db.Customers.Find(customer.Id);
            if (existingCustomer != null)
            {
                _db.Entry(existingCustomer).CurrentValues.SetValues(customer);
                await _db.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
