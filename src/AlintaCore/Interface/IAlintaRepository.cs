﻿using AlintaInfrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AlintaCore.Interface
{
    public interface IAlintaRepository
    {
        Task<bool> AnyCustomer(int id);
        Task<int> CreateCustomer(Customer customer);
        Task<bool> DeleteCustomer(int id);
        Task<Customer> GetCustomer(int id);
        Task<IEnumerable<Customer>> SearchCustomer(string name);
        Task<bool> UpdateCustomer(Customer customer);
        
    }
}
