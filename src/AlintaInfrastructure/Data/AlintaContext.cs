﻿using AlintaInfrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlintaInfrastructure.Data
{
    public class AlintaContext : DbContext
    {
        public AlintaContext(DbContextOptions<AlintaContext> options) : base(options)
        { }

        public DbSet<Customer> Customers { get; set; }
    }
}
