﻿using AlintaCore.Interface;
using AlintaInfrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApi.Controllers;
using Xunit;

namespace AlintaWebApiTest
{
    public class CustomerApiTest
    {
        private CustomerController _apiController;
        
        public CustomerApiTest()
        {
            Mock<IAlintaRepository> repository;
            repository = new Mock<IAlintaRepository>();

            repository.Setup(x => x.AnyCustomer(1)).ReturnsAsync(true);
            repository.Setup(x => x.AnyCustomer(100)).ReturnsAsync(false);
            repository.Setup(x => x.CreateCustomer(It.IsAny<Customer>())).ReturnsAsync(1);
            repository.Setup(x => x.DeleteCustomer(It.IsAny<int>())).ReturnsAsync(true);
            repository.Setup(x => x.GetCustomer(It.IsAny<int>())).ReturnsAsync(new Customer
                {
                    Id = 1,
                    FirstName = "Max",
                    LastName = "Wang",
                    DateOfBirth = new DateTime(1980, 1, 1)
                });
            repository.Setup(x => x.SearchCustomer(It.IsAny<string>())).ReturnsAsync(new List<Customer>
                { 
                    new Customer 
                    { 
                        Id = 1,
                        FirstName = "Max",
                        LastName = "Wang",
                        DateOfBirth = new DateTime(1980, 1, 1) 
                    } 
                });

            _apiController = new CustomerController(repository.Object);
        }

        [Fact]
        public async Task GetCustomerWithWrongIdShouldReturnBadRequest()
        {

            var result = await _apiController.Get(-1);

            var badResult = result as BadRequestResult;

            Assert.Equal(400, badResult.StatusCode);
            
        }

        [Fact]
        public async Task GetCustomerWithNormalShouldReturnOK()
        {

            var result = await _apiController.Get(1);

            var okResult = result as OkObjectResult;

            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task PostNullShouldReturnBadRequest()
        {
            var result = await _apiController.Post(null);
            var badResult = result as BadRequestObjectResult;

            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public async Task PostModelStateInvalidateShouldReturnBadRequest()
        {
            _apiController.ModelState.AddModelError("LastName", "Required");
            var result = await _apiController.Post(new Customer { FirstName = "test" });
            var badResult = result as BadRequestObjectResult;
            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public async Task PutIdNotMatchShouldReturnBadRequest()
        {
            var result = await _apiController.Put(1, new Customer { Id = 2});
            var badResult = result as BadRequestResult;

            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public async Task PutModelNotValidateShouldReturnBadRequest()
        {
            _apiController.ModelState.AddModelError("LastName", "Required");
            var result = await _apiController.Put(1, new Customer { Id = 2 });
            var badResult = result as BadRequestResult;
            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public async Task DeleteWrongIdShouldReturnBadRequest()
        {
            
            var result = await _apiController.Delete(-1);
            var badResult = result as BadRequestResult;
            Assert.Equal(400, badResult.StatusCode);
        }

        [Fact]
        public async Task DeleteNotExistCustomerShouldReturnNotFound()
        {

            var result = await _apiController.Delete(100);
            var notfoundResult = result as NotFoundResult;
            Assert.Equal(404, notfoundResult.StatusCode);
        }

    }
}
