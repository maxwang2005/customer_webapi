﻿using AlintaCore.Repository;
using AlintaInfrastructure.Data;
using AlintaInfrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AlintaCoreUnitTest
{
    public class AlintaDbRepositoryTest : IDisposable
    {
        private readonly AlintaDbRepository _db;
        private readonly AlintaContext _context;
        public AlintaDbRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<AlintaContext>()
                       .UseInMemoryDatabase(databaseName: "InMemoryArticleDatabase")
                       .Options;
            
            _context = new AlintaContext(options);

            _context.Database.EnsureCreated();

            _context.Customers.AddRange(
                   new Customer
                   {
                       Id = 1,
                       FirstName = "James",
                       LastName = "Smith",
                       DateOfBirth = new DateTime(1980, 1, 1)
                   },
                   new Customer
                   {
                       Id = 2,
                       FirstName = "Smith",
                       LastName = "Hernandez",
                       DateOfBirth = new DateTime(1980, 1, 1)
                   },
                   new Customer
                   {
                       Id = 3,
                       FirstName = "Robert",
                       LastName = "Garcia",
                       DateOfBirth = new DateTime(1980, 1, 1)
                   }
            );

            _context.SaveChanges();

            _db = new AlintaDbRepository(_context);
        }

        [Fact]
        public async Task AnyCusotmerShouldReturnFalseWhenIdNotExist()
        {
            Assert.False(await _db.AnyCustomer(5));
        }

        [Fact]
        public async Task AnyCusotmerShouldReturnTrueWhenIdExist()
        {
            Assert.True(await _db.AnyCustomer(1));
        }

        [Fact]
        public async Task CreateCustomerShouldReturnTrue()
        {
            //Id field not working for in memory database
            //please reference InMemory: Improve in-memory key generation
            //https://github.com/aspnet/EntityFrameworkCore/issues/6872
            //pass id here
            var customer = new Customer
            {
                Id = 4, 
                FirstName = "Lock",
                LastName = "Edith",
                DateOfBirth = new DateTime(1980, 1, 1)
            };
            var result = await _db.CreateCustomer(customer);

            Assert.True(result > 0);
        }

        [Fact]
        public async Task DeleteExistingCustomerShouldReturnTrue()
        {
            var result = await _db.DeleteCustomer(1);
            Assert.True(result);
        }

        [Fact]
        public async Task GetExistingCustomerShouldReturnRightValue()
        {
            var customer = await _db.GetCustomer(1);
            Assert.Equal("James", customer.FirstName);
            Assert.Equal("Smith", customer.LastName);
            Assert.Equal(new DateTime(1980, 1, 1), customer.DateOfBirth);
        }


        [Fact]
        public async Task SearchCustomersShouldReturnRightValues()
        {
            var customers = await _db.SearchCustomer("James");
            Assert.NotNull(customers);
            Assert.Single(customers);
            Assert.Equal(1, customers.First().Id);

            customers = await _db.SearchCustomer("Smith");
            Assert.NotNull(customers);
            Assert.Equal(2, customers.Count());
        }

        [Fact]
        public async Task UpdateExistingCustomerShouldReturnTrue()
        {
            var customer = new Customer
            {
                Id = 4,
                FirstName = "Emma",
                LastName = "Mia",
                DateOfBirth = new DateTime(1990, 1, 1)
            };
            
            var result = await _db.UpdateCustomer(customer);

            Assert.True(result);

            customer = await _db.GetCustomer(4);
            Assert.Equal("Emma", customer.FirstName);
            Assert.Equal("Mia", customer.LastName);
            Assert.Equal(new DateTime(1990, 1, 1), customer.DateOfBirth);
        }


        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }
    }
}
